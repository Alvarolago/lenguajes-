
function limpiar(){			//Hacemos un bucle que recorra todas las celdas para eliminar la imagen y el estilo (casillas azules)

	for(cnt=0; cnt<8; cnt++){
		for(cnt2=0; cnt2<8; cnt2++){
			casilla = document.getElementById("c"+cnt2+cnt);
			casilla.innerHTML="";					//Elimina la imagen(figura)
			casilla.style.background = null;		//Elimina el estilo de fondo(celdas "amenazadas")
		}
	}
}


function comprueba(fila,columna){
		if (fila < 0 || fila > 7) {											//Alerta en caso de que el usuario introduzca una columna no válida
		alert("Intruduzca una columna válida entre 1 y 8");
		return false;
	}

	if (columna < 0 || columna > 7) {											//Alerta en caso de que el usuario introduzca una columna no válida
		alert("Intruduzca una columna válida entre 1 y 8");
		return false;
	}
}

function blanconegro(){															//Función que permite elegir entre la pieza blanca o negra
		if(document.getElementById("blanco").checked){					//Condición si el botón está marcado	
		bn = "B";
	}
	else{

		bn="A";
	}
}



function generar(){

	limpiar();															//Llamamos a la función limpiar al principio de la función para eliminar los rastros anteriores


	fila = parseInt(document.getElementById('fila').value);				//Almacenamos en 2 variables distintas la fila y la columna indicada por el usuario
	columna = (document.getElementById('columna').value);

	letras();

	fila = fila - 1;													//Restamos 1 a las variables para que no comienze por a contar las filas/columnas por 0.
	columna = columna -1;


	comprueba(fila,columna);

	blanconegro();


	if(document.getElementById("peon").selected == true){				//Dependiendo de la figura seleccionada por el usuario en la etiqueta deplegable imprimimos la figura indicada
																		// Para simplificar usamos la siguiente notación: 
																		// Peon = 0		|	Torre = 1	|	Alfil = 2	|	Caballo = 3		|	Rey = 4		|	Reina = 5
																		// De esta forma llamamos a la función amenaza(imprime celdas azules) pasandole la figura seleccionada
	casilla = document.getElementById("c"+fila+columna);
	casilla.innerHTML="<img src='imagenes/piezas/peon"+bn+".jpg'></img>";

		amenaza(0);
	}

	else if(document.getElementById("torre").selected == true){

	casilla = document.getElementById("c"+fila+columna);
	casilla.innerHTML="<img src='imagenes/piezas/torre"+bn+".jpg'></img>";

		amenaza(1);
	}

	else if(document.getElementById("alfil").selected == true){

	casilla = document.getElementById("c"+fila+columna);
	casilla.innerHTML="<img src='imagenes/piezas/alfil"+bn+".jpg'></img>";

		amenaza(2);
	}

	else if(document.getElementById("caballo").selected == true){

	casilla = document.getElementById("c"+fila+columna);
	casilla.innerHTML="<img src='imagenes/piezas/caballo"+bn+".jpg'></img>";

		amenaza(3);
	}

	else if(document.getElementById("rey").selected == true){

	casilla = document.getElementById("c"+fila+columna);
	casilla.innerHTML="<img src='imagenes/piezas/rey"+bn+".jpg'></img>";

		amenaza(4);
	}

	else if(document.getElementById("reina").selected == true){

	casilla = document.getElementById("c"+fila+columna);
	casilla.innerHTML="<img src='imagenes/piezas/reina"+bn+".jpg'></img>";

		amenaza(5);
	}

	else{
		alert("No select");
	}

}

function amenaza(pieza){								//Hacemos un if/else para cada figura ya que las celdas amenazadas son distintas en cada pieza

	if(pieza == 0){					//PEON

		peon();
	}

	else if(pieza == 1){			//TORRE

		torre();
	}

	else if(pieza == 2){			//ALFIL

 		alfil();
	}

	else if (pieza == 3){			//CABALLO

		caballo();
	}

	else if(pieza == 4){			//REY

		rey();

	}

	else if(pieza == 5){
		rey();
		torre();
		alfil();



	
	


	}
}