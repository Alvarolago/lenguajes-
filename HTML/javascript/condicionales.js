function CalcularMayor()
{
	var n1,
		n2,
		resultado;
		
// parseInt transforma la cadena a int en vez de char que es lo que está por defecto


	n1 = parseInt(document.getElementById('numero1').value);
	n2 = document.getElementById('numero1').value;
	resultado = document.getElementById('resultado');
	if(n1 > n2)
		resultado.innerHTML += "<br> El mayor es " + n1;
	else
		resultado.innerHTML += "<br> El mayor es " + n2;


}

function CalculaDiasMes()
{
	var mesCadena;
	mes;

	mesCadena = document.getElementById('numeroMes').value;
	mes = parseInt(mesCadena);

	switch(mes)
	{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12: diasMes = 31;
				break;
		case 4:
		case 6:
		case 9:
		case 11: diasMes = 30;
				break;
		case 2: diasMes = 28;
				break;
		default: diasMes = 99;
				break;
	}
	resultado = document.getElementById('resultado');
	resultado.innerHTML +=
	"El mes " + mes + " tiene " + diasMes + "dias.";
}